const router = require('express-promise-router')();
const authRoutes = require('./auth');


router.get('/', (req, res) => {
  res.render('index');
});

router.use('/auth', authRoutes);


module.exports = router;
