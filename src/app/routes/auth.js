const router = require('express').Router();
const { login } = require('../controllers/auth/LoginController');
const { register } = require('../controllers/auth/RegisterController');


router.get('/', (req, res) => {
  res.redirect('/auth/login');
});

router.get('/login', login);
router.get('/register', register);

module.exports = router;
