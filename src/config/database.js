const mongoose = require('mongoose');

mongoose
  .connect('mongodb://localhost/node-app')
  .then(() => {
    console.log('mongodb is connected');
  })
  .catch(err => {
    console.log(err);
  });
