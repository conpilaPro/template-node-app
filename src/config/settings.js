const express = require('express');
const path = require('path');

module.exports = app => {

  app.set('port', process.env.PORT || 8080);
  app.set('view engine', 'ejs');
  app.set('views', path.join(__dirname + '/../resources/views'));

  app.use(express.static(path.join(__dirname + '/../public')));

};
