const app = require('express')();

// settings
require('./config/settings')(app);

// database
require('./config/database');

// middlewares
require('./config/middlewares')(app);

// routes
const  routes = require('./app/routes/index');
app.use('/', routes);

// boot
require('./config/boot')(app);
